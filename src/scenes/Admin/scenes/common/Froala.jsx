import React from 'react';
import _ from 'lodash';
// Require Editor JS files.
import 'froala-editor/js/froala_editor.pkgd.min.js';
import $ from 'jquery';
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
// Require Font Awesome.
import 'font-awesome/css/font-awesome.css';

import FroalaEditor from 'react-froala-wysiwyg';

class Froala extends React.Component {
    options = {
        toolbarStickyOffset: 65,
        fontFamily: {
            'Georgia,serif': 'Georgia',
            "'Times New Roman',Times,serif": 'Times New Roman'
        },
        imageDefaultWidth: 0,
        fontFamilyDefaultSelection: 'Georgia',
        fontSizeDefaultSelection: '21',
        placeholder: 'Edit Me',
        imageUploadParam: 'image',
        imageUploadRemoteUrls: true,
        imageUploadURL: `${API_URL}/upload/image`,
        events: {
            'froalaEditor.image.uploaded': (e, editor, res) => {
                res = JSON.parse(res);
                this.props.updateStateImages([...this.props.images, res.filename]);
            },
            'froalaEditor.image.removed': (e, editor, $img) => {
                const image = $img.attr('data-filename');
                this.props.deleteImages([image]);
            },
            'froalaEditor.image.error': (e, editor, err) => {
                console.log(err);
            }
        }
    };
    render() {
        const { input: { value, onChange } } = this.props;
        return (
            <FroalaEditor
                tag="textarea"
                model={value}
                onModelChange={model => onChange(model)}
                config={this.options}
            />
        );
    }
}

export default Froala;
