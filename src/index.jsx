import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import axios from 'axios';
import thunk from 'redux-thunk';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import allReducers from './services/index';
import App from './scenes/App/App';
import Admin from './scenes/Admin/Admin';
import SignIn from './scenes/Sign/Login/Login';
import { AUTH_USER } from './services/types';

const store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk)
);

function startApplication() {
    ReactDOM.render(
        <Provider store={store}>
            <Router>
                <MuiThemeProvider>
                    <Switch>
                        <Route path="/admin" component={Admin} />
                        <Route path="/signin" component={SignIn} />
                        <Route path="/" component={App} />
                    </Switch>
                </MuiThemeProvider>
            </Router>
        </Provider>,
        document.getElementById('root')
    );
}

const token = localStorage.getItem('token');
if (token) {
    axios.post(`${API_URL}/verify`, { token })
        .then(() => {
            store.dispatch({ type: AUTH_USER });
            startApplication();
        })
        .catch(() => {
            startApplication();
        });
} else {
    startApplication();
}
