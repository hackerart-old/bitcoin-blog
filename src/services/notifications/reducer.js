import { NEW_NOTIFICATION } from '../types';

export default function (state = null, action) {
    switch (action.type) {
        case NEW_NOTIFICATION:
            return action.payload;
        default:
            return state;
    }
}
