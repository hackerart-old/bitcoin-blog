import React from 'react';
import styles from './Banner.scss';

const Banner = () => {
    return (
        <section className={styles.banner}>
            <div className="container">
                <h1> BITCOIN NEWS </h1>
            </div>
        </section>
    );
};

export default Banner;
