import _ from 'lodash';
import {
    NEW_POST,
    FETCH_POST,
    FETCH_POSTS,
    DELETE_POST
} from '../types';

const initialState = {
    list: {},
    amount: 0
};

export default function (state = initialState, action) {
    switch (action.type) {
        case NEW_POST:
        case FETCH_POST:
            return {
                ...state,
                list: {
                    [action.payload._id]: action.payload
                }
            };
        case FETCH_POSTS:
            const { posts, amount } = action.payload;
            return {
                amount,
                list: _.mapKeys(posts, '_id')
            };
        case DELETE_POST:
            return { ...state, list: _.omit(state.list, action.payload) };
        default:
            return state;
    }
};
