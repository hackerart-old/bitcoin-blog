/* eslint-disable import/no-extraneous-dependencies,prefer-destructuring */
/* eslint-disable no-console */
import express from 'express';
import compression from 'compression';
import pug from 'pug';
import path from 'path';
import webpack from 'webpack';
import open from 'open';
import nodemailer from 'nodemailer';
import bodyParser from 'body-parser';

import config from '../webpack.dev';

const port = 9000;
const app = express();

app.use(compression());

const compiler = webpack(config);
//  Here we are configuring express to use body-parser as middle-ware.

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'email'));

app.use(express.static(path.join(__dirname, 'email')));

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/assets/favicon.ico', (req, res) => {
    res.sendFile(path.join(__dirname, '../img/favicon.png'));
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../src/index.html'));
});

app.listen(port, (err) => {
    if (err) {
        console.log(err);
    } else {
        open(`http://localhost:${port}`);
        console.log(`http://localhost:${port}`);
    }
});

