import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
    button: {
        margin: 12,
        marginTop: 50
    },
    imageInput: {
        cursor: 'pointer',
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        width: '100%',
        opacity: 0
    }
};

const FileInput = ({ input: { onChange }, label }) => {
    return (
        <RaisedButton
            label={label}
            labelPosition="before"
            style={styles.button}
            containerElement="label"
        >
            <input
                onChange={(e) => {
                    e.preventDefault();
                    onChange(e.target.files[0]);
                }}
                type="file"
                style={styles.imageInput}
            />
        </RaisedButton>
    );
};

export default FileInput;
