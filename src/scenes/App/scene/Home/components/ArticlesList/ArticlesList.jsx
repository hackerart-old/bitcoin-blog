import map from 'lodash/map';
import React from 'react';
import styles from './ArticlesList.scss';
import ArticlePreview from './components/ArticlePreview/ArticlePreview';
import Pagination from '../../../../../../common/Pagination/Pagination';
import * as actions from '../../../../../../services/posts/actions';

const ArticlesList = ({ data }) => {
    return (
        <section className={styles['article-list']}>
            <div className="container">
                <div className={['row', styles.row].join(' ')}>
                    {map(data, post =>
                        <ArticlePreview key={post._id} {...post} />)}
                </div>
            </div>
        </section>
    );
};

export default Pagination({
    perPage: 6,
    loadDataAction: actions.fetchPosts,
    appStateName: 'posts'
})(ArticlesList);
