import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import postsReducer from './posts/reducer';
import authReducer from './auth/reducer';
import notificationsReducer from './notifications/reducer';

export default combineReducers({
    form: formReducer,
    posts: postsReducer,
    auth: authReducer,
    notifications: notificationsReducer
});
