import React from 'react';
import { connect } from 'react-redux';
import FroalaEditorView from 'react-froala-wysiwyg/FroalaEditorView';
import styles from './Article.scss';
import * as actions from '../../../../services/posts/actions';

class ArticleFull extends React.Component {
    componentDidMount() {
        this.props.fetchPost(this.props.match.params.id);
    }
    render() {
        if (!this.props.post) { return <div>loading...</div>; }
        const { title, content, description } = this.props.post;
        return (
            <section className={styles.container}>
                <h1>{title}</h1>
                <h2>{description}</h2>
                <FroalaEditorView
                    model={content}
                />
            </section>
        );
    }
}

const mapState = ({ posts }, ownProps) => {
    return { post: posts.list[ownProps.match.params.id] };
};

export default connect(mapState, actions)(ArticleFull);
