import React from 'react';
import {
    TableRow,
    TableRowColumn
} from 'material-ui/Table';
import moment from 'moment';
import { red500, yellow800 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import EditIcon from 'material-ui/svg-icons/image/edit';

function handleOnClick() {
    this.func(this.data);
}

const PostsTableRow = ({ post, deletePost }) => {
    const btn = { width: '72px' };
    return (
        <TableRow>
            <TableRowColumn>{post.title}</TableRowColumn>
            <TableRowColumn>{post.description}</TableRowColumn>
            <TableRowColumn style={{ width: '150px' }}>
                {moment(post.createdAt).format('ll')}
            </TableRowColumn>
            <TableRowColumn style={btn}>
                <IconButton>
                    <EditIcon color={yellow800} />
                </IconButton>
            </TableRowColumn>
            <TableRowColumn style={btn}>
                <IconButton
                    onClick={handleOnClick.bind({
                        func: deletePost,
                        data: post._id
                    })}
                >
                    <DeleteIcon color={red500} />
                </IconButton>
            </TableRowColumn>
        </TableRow>
    );
};

export default PostsTableRow;
