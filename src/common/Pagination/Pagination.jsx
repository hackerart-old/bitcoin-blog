import React from 'react';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { scroller, Element } from 'react-scroll';

export default function (options = {}) {
    const { perPage, loadDataAction, appStateName } = options;
    return function (Component) {
        class Pagination extends React.Component {
            state = {
                currentPage: 0
            };
            componentDidMount() {
                this.update();
            }

            update = () => {
                const query = `page=${this.state.currentPage + 1}`;
                this.props.loadDataAction(query, (data) => {
                    if (data.length < 1 && this.state.currentPage > 0) {
                        this.setState({ currentPage: this.state.currentPage - 1 }, () => {
                            this.update();
                        });
                    }
                });
            }

            handlePageClick = ({ selected }) => {
                this.setState({ currentPage: selected }, () => {
                    const query = `page=${selected + 1}`;
                    this.props.loadDataAction(query, () => {
                        scroller.scrollTo('myScrollToElement', {
                            duration: 500,
                            delay: 100,
                            smooth: true,
                            offset: -50
                        });
                    });
                });
            };

            render() {
                return (
                    <div>
                        <Element name="myScrollToElement" />
                        <Component data={this.props.data} update={this.update} />
                        <ReactPaginate
                            previousLabel="Previous"
                            nextLabel="Next"
                            breakLabel={<a href="">...</a>}
                            breakClassName="break-me"
                            pageCount={Math.ceil(this.props.amount/perPage)}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName="pagination justify-content-center"
                            activeClassName="active"
                            disabledClassName="disabled"
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            nextClassName="page-item"
                            previousLinkClassName="page-link"
                            nextLinkClassName="page-link"
                        />
                    </div>
                );
            }
        }
        const mapState = (state) => {
            const { list, amount } = state[appStateName];
            return {
                data: list,
                amount
            };
        };
        return connect(mapState, { loadDataAction })(Pagination);
    };
};
