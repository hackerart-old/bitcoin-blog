import axios from 'axios';
import { newNotification } from '../notifications/actions';
import {
    AUTH_USER,
    DEAUTH_USER,
    AUTH_ERROR
} from '../types';

export function authenticate(type, data, history, cb) {
    return dispatch => {
        axios.post(`${API_URL}/${type}`, data)
            .then(({ data }) => {
                localStorage.setItem('token', data.token);
                dispatch({ type: AUTH_USER });
                dispatch(newNotification(`Welcome Admin`));
                history.push('/admin');
            })
            .catch(err => {
                if (typeof cb === 'function') { cb(); }
                const res = err.response;
                const errorMessage =  res ? res.data.message : err.message;
                dispatch(authError(errorMessage));
            });
    };
}

export function deauthenticate(history) {
    localStorage.removeItem('token');
    history.push('/signin');
    return { type: DEAUTH_USER };
}

export function authError(message) {
    return {
        type: AUTH_ERROR,
        payload: message
    };
}
