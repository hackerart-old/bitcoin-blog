import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Media from 'react-media';
import { reduxForm, Field } from 'redux-form';
import axios from 'axios';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import styles from './NewPost.scss';
import * as actions from '../../../../services/posts/actions';
import Froala from '../common/Froala';
import FileInput from '../common/FileInput';

class NewPost extends React.Component {
    state = {
        images: [],
        sent: false,
        offsetY: 0
    };
    componentDidMount() {
        window.addEventListener('beforeunload', this.componentCleanUp);
        window.onscroll = this.updateOffset;
    }
    componentWillUnmount() {
        this.componentCleanUp();
    }
    componentCleanUp = () => {
        const { images, sent } = this.state;
        if (!sent && images) { this.deleteImages(images); }
        window.removeEventListener('onscroll', this.updateOffset);
        window.removeEventListener('beforeunload', this.componentCleanUp);
    };
    updateOffset = () => {
        if (window.innerWidth > 992) {
            const offsetY = window.pageYOffset || document.documentElement.scrollTop;
            this.setState({ offsetY });
        }
    };
    updateStateImages = images => this.setState({ images });
    deleteImages = images => {
        this.setState({
            images: _.xor(this.state.images, images)
        });
        axios.post(`${API_URL}/delete/images`, { images })
            .then(res => console.log('Images deleted'))
            .catch(err => console.log('error occured'));
    };
    send = (values) => {
        values.froalaImages = this.state.images;
        this.props.newPost(values, this.props.history, () => {
            this.setState({ sent: true });
        });
    };
    renderInput({ input, options, meta }) {
        const { touched, error } = meta;

        return (
            <TextField
                errorText={touched ? error : ''}
                {...options}
                {...input}
            />
        );
    }
    renderButtons() {
        return (
            <div className={styles.buttons}>
                <RaisedButton
                    label="Submit"
                    type="submit"
                    primary={true}
                    style={{ margin: 12 }}
                />
                <Link to="/admin"><FlatButton label="Cancel" /></Link>
            </div>
        );
    }
    render() {
        const { handleSubmit } = this.props;
        return (
            <div className={styles['new-post']}>
                <h2> Add Post </h2>
                <form onSubmit={handleSubmit(this.send)}>
                    <div className={styles.formFields}>
                        <div
                            className={styles.left}
                            style={{ marginTop: this.state.offsetY }}
                        >
                            <Field
                                name="title"
                                options={{
                                    hintText: 'Enter title',
                                    floatingLabelText: 'Title'
                                }}
                                component={this.renderInput}
                            />
                            <Field
                                name="description"
                                options={{
                                    hintText: 'Enter decription',
                                    floatingLabelText: 'Decription',
                                    multiLine: true,
                                    rows: 2,
                                    rowsMax: 4
                                }}
                                component={this.renderInput}
                            />
                            <Field
                                name="imagePreview"
                                label="Select preview image"
                                component={FileInput}
                            />
                            <Media
                                query="(min-width: 992px)"
                                render={this.renderButtons}
                            />
                        </div>
                        <div className={styles.right}>
                            <Field
                                name="content"
                                component={Froala}
                                deleteImages={this.deleteImages}
                                images={this.state.images}
                                updateStateImages={this.updateStateImages}
                            />
                        </div>
                    </div>
                    <Media
                        query="(max-width: 991px)"
                        render={this.renderButtons}
                    />
                </form>
            </div>
        );
    }
}

export default reduxForm({
    form: 'newPost'
})(connect(null, actions)(NewPost));
