import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import AddIcon from 'material-ui/svg-icons/content/add';
import HomeIcon from 'material-ui/svg-icons/action/home';
import WebIcon from 'material-ui/svg-icons/av/web';
import styles from './LeftBar.scss';

const list = [
    { to: '/admin', Icon: HomeIcon, title: 'Dashboard' },
    { to: '/admin/posts/new', Icon: AddIcon, title: 'Create new post' },
    { to: '/', Icon: WebIcon, title: 'Go to website' }
];

const LeftBar = ({ collapsed, closeSidebar }) => {
    return (
        <div
            className={[
                styles.sidebar,
                collapsed ? styles['is-collapsed'] : ''
            ].join(' ')}
        >
            <div className={styles.logo}>
                <Link to="/admin">
                    <div className={styles['logo-group']}>
                        <div className={styles['logo-image']}>
                            <img src={require('../../../../../img/logo.png')} alt="Bitcoin" />
                        </div>
                        <h1>Bitcoin Blog</h1>
                    </div>
                </Link>
            </div>
            <div className={styles.menu}>
                <ul>
                    {list.map(({ to, Icon, title }, index) =>
                        <li key={index} className={styles['nav-item']} onClick={closeSidebar}>
                            <NavLink exact to={to} activeClassName={styles.active}>
                                <span className={styles['icon-holder']}>
                                    <Icon />
                                </span>
                                <span className={styles.title}>{title}</span>
                            </NavLink>
                        </li>
                    )}
                </ul>
            </div>
        </div>
    );
};


export default LeftBar;
